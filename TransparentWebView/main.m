//
//  main.m
//  TransparentWebView
//
//  Created by Pawel Müller on 11.03.14.
//  Copyright (c) 2014 Anacope. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
