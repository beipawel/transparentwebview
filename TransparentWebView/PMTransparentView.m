//
//  PMTransparentView.m
//  TransparentWebView
//
//  Created by Pawel Müller on 11.03.14.
//  Copyright (c) 2014 Anacope. All rights reserved.
//

#import "PMTransparentView.h"

@implementation PMTransparentView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
	
    // Clear the drawing rect.
    [[NSColor clearColor] set];
    NSRectFill([self frame]);
//    [self.window display];

}

@end
