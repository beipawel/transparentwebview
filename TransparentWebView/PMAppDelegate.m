//
//  PMAppDelegate.m
//  TransparentWebView
//
//  Created by Pawel Müller on 11.03.14.
//  Copyright (c) 2014 Anacope. All rights reserved.
//

#import "PMAppDelegate.h"
#import "PMTransparentView.h"
#import <WebKit/WebKit.h>


@implementation PMAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"Resources"];
    NSURL* fileURL = [NSURL fileURLWithPath:filePath];
    NSURLRequest* request = [NSURLRequest requestWithURL:fileURL];
    [self.webview.mainFrame loadRequest:request];
    
    [self.webview setDrawsBackground:NO];
//    [self.window setOpaque:NO];
}

@end
